# Branch "main" - Project shiny-k8s

## Project Goal

To help members of Bioinformatic & Biostatistic Hub and Institut Pasteur to correctly and easily deploy a web application, developed in Shiny ( with R or Python), under a Kubernetes cluster managed by the IT department, we have developed the shiny-k8s-toolkit using gitlab projects containig all the components to be used by user or developper to deploy their application.

Dear User, who develop Shiny application, please go to the dedicated project [shiny-k8s-example](https://gitlab.pasteur.fr/hub/shiny-k8s-example) containing example of using the components developped here. Please follow detailed instructions in the [User Guide](https://hub.pages.pasteur.fr/shiny-k8s/user_guide/index.html)

Dear Developer,you can stay here if you want to know how the shiny-k8s-toolkit is built. More information for developer in the [Developer Guide](https://hub.pages.pasteur.fr/shiny-k8s/developer_guide/index.html)

## Project Structure

This project use orphan branch concept [^1] to manage the different ressources using according to the usage.

```ini
shiny-k8s
  ├── main
  ├── helm              ### Dedicated Orphan branch to store helm template and help to manage the kubernetes configuration
  ├── docker-images     ### Dedicated orphan branch to build docker images will be used in examples
  └── docs              ### Dedicated orphan branch to host the sphinx documentation of toolkit
```

[^1]: [git definition](https://git-scm.com/docs/git-checkout/2.14.6#Documentation/git-checkout.txt---orphanltnewbranchgt)

## Documentation

All informations are available in [Documentation](https://hub.pages.pasteur.fr/shiny-k8s/)
